# Tinder App


Tinder App fue un pequeño proyecto creado para revisar las características que ofrece Tinder dentro de una aplicación web.

### Contexto

Surgio mi curiosidad cuando leí el siguiente artículo [UN INFORMÁTICO EN EL LADO DEL MAL](https://www.elladodelmal.com/2016/08/ojo-con-los-acosadores-en-tinder-en.html) 
El artículo menciona que cuando dos personas hacen match la app envia la ubicación GPS de tu match. Bajo esta premisa se puede hacer un seguimiento a una persona creando bots en tinder y con la tecnica de trilateración.

Mientras desarrollaba este proyecto la aplicación Tinder parchó este problema de seguridad.
Por lo cual el proyecto solo quedó en obtener un album de fotos que se encuentran cerca de la ubicación que configures.


### Tecnología

Tinder App usa y esta basado en: 

* [PHP]
* [Tinder API Documentation](https://github.com/fbessez/Tinder)


### Instalación

No se recomienda :P

### Video
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/J1ANlw-S47c/0.jpg)](https://www.youtube.com/watch?v=J1ANlw-S47c)