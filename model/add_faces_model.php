<?php

/**
 *
 */
class AddFaces_Model
{

    private $conn;
    private $faces;

    function __construct()
    {
        require_once("model/connect.php");
        require_once("model/api/api_tinder.php");

        $this->conn = Connect::connection();
        $this->log =  array();
    }


    public function sort_information()
    {
        $tinder = new Tinder('100001501387778', 'EAAGm0PX4ZCpsBAOgwVA4G5nPyouVFYywmgvuXcTwSVSKYfq2UmyFIsb2LWKyQD4L4VgxtUIFna78plsOMZBUyPypb1r0wVIvorv09aPCFf8OtlMMO0A4Kvk69fFep4TrHWZBJmIPMIpfcb30ikRxurF8OnESSi6DwIomAXDxh6iSV2m9FyMI6ow63y1hvXaMXQBm74cB9sGeGsgIgGm');

        $recommendations = $tinder->recommendations();

        if (isset($recommendations['results'])) {
            for ($i = 0; $i < count($recommendations['results']); $i++) {
                $_id = $recommendations['results'][$i]['_id'];
                $d_name = $recommendations['results'][$i]['name'];
                $d_bio = $recommendations['results'][$i]['bio'];
                $d_gender = $recommendations['results'][$i]['gender'];
                $d_distance = $recommendations['results'][$i]['distance_mi'];
                $d_birthdate = substr($recommendations['results'][$i]['birth_date'], 0, 10);
                $d_urlPhoto = $recommendations['results'][$i]['photos'][0]['url'];


                $bio_quote = $this->conn->quote($d_bio);
                try {
                    $sql = "INSERT IGNORE INTO data (_id, name, bio, gender, distance, birth_date, urlPhoto)
                            VALUES ('$_id', '$d_name', $bio_quote, '$d_gender', $d_distance, '$d_birthdate', '$d_urlPhoto')";

                    $ins_test = $this->conn->prepare($sql);
                    $ins_test->execute();
                } catch (PDOException $e) {
                    echo $e->getMessage();
                    die();
                }


                if ($this->conn->query("SHOW WARNINGS")->fetch() == FALSE) {

                    if (isset($recommendations['results'][$i]['instagram'])) {

                        $i_username = $recommendations['results'][$i]['instagram']['username'];

                        $i_lastconn = $recommendations['results'][$i]['instagram']['last_fetch_time'];

                        #$iProfilePicture=$recommendations['results'][$i]['instagram']['profile_picture'];

                        $stmt = $this->conn->prepare('INSERT INTO instagram (_id, username, last_conn) VALUES (:_id, :username, :last_conn);');
                        $stmt->bindParam(':_id', $_id, PDO::PARAM_STR);
                        $stmt->bindParam(':username', $i_username, PDO::PARAM_STR);
                        $stmt->bindParam(':last_conn', $i_lastconn, PDO::PARAM_STR);

                        try {
                            $stmt->execute();
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                            die();
                        }
                    }

                    for ($k = 0; $k < count($recommendations['results'][$i]['jobs']); $k++) {

                        $stmt = $this->conn->prepare('INSERT INTO job (_id, company, title) VALUES (:_id, :company, :title);');
                        $stmt->bindParam(':_id', $_id, PDO::PARAM_STR);

                        if (!empty($recommendations['results'][$i]['jobs'][$k]['company']['name'])) {
                            $j_company = "\"" . $recommendations['results'][$i]['jobs'][$k]['company']['name'] . "\"";
                            $stmt->bindParam(':company', $j_company, PDO::PARAM_STR);
                        } else {
                            $stmt->bindValue(':company', NULL, PDO::PARAM_INT);
                        }

                        if (!empty($recommendations['results'][$i]['jobs'][$k]['title']['name'])) {
                            $j_title = "\"" . $recommendations['results'][$i]['jobs'][$k]['title']['name'] . "\"";
                            $stmt->bindParam(':title', $j_title, PDO::PARAM_STR);
                        } else {
                            $stmt->bindValue(':title', NULL, PDO::PARAM_INT);
                        }

                        try {
                            $stmt->execute();
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                            die();
                        }
                    }

                    for ($l = 0; $l < count($recommendations['results'][$i]['schools']); $l++) {
                        if (!empty($recommendations['results'][$i]['schools'][$l]['name'])) {
                            $stmt = $this->conn->prepare('INSERT INTO school (_id, name_sc) VALUES (:_id, :name_sc);');
                            $stmt->bindParam(':_id', $_id, PDO::PARAM_STR);

                            $s_name = $recommendations['results'][$i]['schools'][$l]['name'];
                            $stmt->bindParam(':name_sc', $s_name, PDO::PARAM_STR);

                            try {
                                $stmt->execute();
                            } catch (PDOException $e) {
                                echo $e->getMessage();
                                die();
                            }
                        }
                    }


                    $contador_fotos = 0;
                    for ($j = 0; $j < count($recommendations['results'][$i]['photos']); $j++) {
                        $stmt = $this->conn->prepare('INSERT INTO photos (_id, id_photo) VALUES (:_id, :id_photo);');
                        $stmt->bindParam(':_id', $_id, PDO::PARAM_STR);

                        $p_idphoto = $recommendations['results'][$i]['photos'][$j]['id'];
                        $stmt->bindParam(':id_photo', $p_idphoto, PDO::PARAM_STR);


                        try {
                            if ($stmt->execute()) {
                                $contador_fotos++;
                            }
                        } catch (PDOException $e) {
                            echo $e->getMessage();
                            die();
                        }
                    }

                    $flag_add_faces = "Successfully";
                } else {
                    $error = $this->conn->query("SHOW WARNINGS")->fetch();
                    $flag_add_faces = $error["Message"];
                }

                #LOG
                $this->log[] = (object) array(
                    'id' => $_id,
                    'name' => $d_name,
                    'distance' => $d_distance,
                    'birthdate' => $d_birthdate,
                    'flag' => $flag_add_faces,
                    'photo' => $recommendations['results'][$i]['photos'][0]['id']
                );
            }
        } else {
            $this->log[] = "PASARON 30 SEGUNDOS Y NADA";
            echo $this->log[0];
        }

        return $this->log;
    }
}
